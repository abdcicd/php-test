FROM php:8.2-fpm

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=UTC

RUN apt update

RUN apt install -y sudo nano

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN useradd -G www-data,root,sudo -u 1333 -d /home/abd abd && echo "abd:abd" | chpasswd
RUN mkdir -p /home/abd/.composer && \
    chown -R abd:abd /home/abd

COPY . ./var/www/html
WORKDIR /var/www

USER abd
